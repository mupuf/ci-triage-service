#!/usr/bin/env python
# -*- mode: python -*-
# Copyright (C) 2022 Igalia, S.L.
"""CI triaging tool."""

import argparse
from collections import defaultdict
from collections.abc import Iterable
from datetime import datetime, timedelta
from dataclasses import field, asdict, dataclass as dc, InitVar
from enum import IntFlag, auto, Enum
import functools
from functools import partial, cached_property
import glob
import io
import os
import pickle
from pprint import pformat  # noqa
from pprint import pprint as pp  # noqa
import re
import shutil
import subprocess
import sys
import time
from typing import Any, Dict, List, TextIO, Optional
import traceback
import unicodedata
import xml  # noqa
import xml.etree.ElementTree as ET  # noqa

import gitlab  # type: ignore
import gitlab.v4.objects
import humanize  # type: ignore
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
import jinja2
from pydantic import field_validator
from pydantic.dataclasses import dataclass
import yaml

from irc import IRCClient
from influx import InfluxDeadmanCheck, InfluxDB
from triage_patterns import TriageResult, TriageType
from util import PathType


class LogLevel(IntFlag):
    ERROR = auto()
    INFO = auto()
    DEBUG = auto()


LOG_LEVEL = LogLevel.INFO


def _log(level, msg, meta=True, **kwargs):
    global LOG_LEVEL
    if level <= LOG_LEVEL:
        if meta:
            ts = datetime.utcnow().strftime("%c|")
            print(ts, msg, **kwargs)
        else:
            print(msg, **kwargs)


def elog(msg, meta=True, **kwargs):
    _log(LogLevel.ERROR, msg, meta, **kwargs)


def ilog(msg, meta=True, **kwargs):
    _log(LogLevel.INFO, msg, meta, **kwargs)


def dlog(msg, meta=True, *args, **kwargs):
    _log(LogLevel.DEBUG, msg, meta, **kwargs)


def rfc3339_format(dt: datetime) -> str:
    return dt.isoformat("T") + "Z"


def timedelta_from_args_delta(delta_str: str) -> timedelta:
    n, units = delta_str.split(" ")
    unit_n = int(n.strip())
    if units.strip().lower().startswith("minute"):
        return timedelta(minutes=unit_n)
    elif units.strip().lower().startswith("hour"):
        return timedelta(hours=unit_n)
    elif units.strip().lower().startswith("day"):
        return timedelta(days=unit_n)
    elif units.strip().lower().startswith("week"):
        return timedelta(weeks=unit_n)
    else:
        raise ValueError(delta_str)


@dataclass
class ConfigGitlab:
    instance_url: str
    project_access_token: str
    project_id: int

    @field_validator('project_access_token')
    def access_token_from_variable(cls, v):
        if v[0] == '$':
            varname = v.removeprefix('$')
            if varval := os.environ.get(varname):
                v = varval
        return v


@dataclass
class ConfigJobBranch:
    allowlist: list[str] = field(default_factory=list)
    rejectlist: list[str] = field(default_factory=list)

    def is_allowed(self, branch_name):
        if any(re.match(branch_name, rej) for rej in self.rejectlist):
            return False

        if len(self.allowlist) > 0:
            return any(re.match(allow, branch_name) for allow in self.allowlist)
        else:
            return True


@dataclass
class ConfigJob:
    name: str
    stage: str
    log_format: str
    branches: Optional[ConfigJobBranch] = None

    def is_job_selectable(self, job):
        return re.match(self.name, job.name) and re.match(self.stage, job.stage) and (self.branches is None or self.branches.is_allowed(job.pipeline['ref']))


@dataclass
class ConfigProject:
    gitlab: ConfigGitlab
    jobs: Dict[str, ConfigJob]

    def find_compatible_jobs(self, branch_name):
        return [j for j in self.jobs.values() if j.branches is None or j.branches.is_allowed(branch_name)]


@dataclass
class ConfigCache:
    max_size_in_bytes: int


@dataclass
class ConfigInfluxDeadmanCheck:
    hostname: str
    bucket: str
    last_seen_seconds: int

    def to_influx_check(self):
        return InfluxDeadmanCheck(**asdict(self))


@dataclass
class ConfigInfluxChecks:
    deadman: List[ConfigInfluxDeadmanCheck] = field(default_factory=list)

    def to_influx_checks(self):
        checks = []
        for check in self.deadman:
            checks.append(check.to_influx_check())
        return checks


@dataclass
class ConfigInfluxDB:
    url: str
    org: str
    bucket: str
    access_token: str
    checks: ConfigInfluxChecks = None

    @field_validator('access_token')
    def access_token_from_variable(cls, v):
        if v[0] == '$':
            varname = v.removeprefix('$')
            if varval := os.environ.get(varname):
                v = varval
        return v

    @field_validator('checks')
    def enforce_non_empty_checks(cls, v):
        if v is None:
            v = ConfigInfluxChecks()
        return v


# @dataclass
# class ConfigSMTP:
#     hostname: str
#     port: int
#     ssl: bool
#     username: str
#     password: str
#
#
# @dataclass
# class ConfigEmail:
#     smtp: ConfigSMTP
#
#     title: str
#     to: List[str]


@dataclass
class ConfigNotificationIRC:
    hostname: str
    port: int
    nickname: str
    channel: str
    ping: List[str] = field(default_factory=list)


@dataclass
class ConfigNotifications:
    irc: Dict[str, ConfigNotificationIRC] = field(default_factory=dict)


@dataclass
class Config:
    projects: dict[str, ConfigProject]
    cache: ConfigCache
    notifications: ConfigNotifications
    influxdb: ConfigInfluxDB = None

    @classmethod
    def from_file(cls, file_path):
        with open(file_path, 'r') as f:
            data = yaml.safe_load(f)
            return cls(**data if data else {})


class Cache:
    def __init__(self, cache_folder):
        self.cache_folder = cache_folder

    def list_keys(self, keys: List[str]):
        try:
            return next(os.walk(os.path.join(self.cache_folder, *keys)))[1]
        except StopIteration:
            return []

    def list_resources(self, keys: List[str]):
        try:
            return next(os.walk(os.path.join(self.cache_folder, *keys)))[2]
        except StopIteration:
            return []

    def exists(self, keys: List[str], filename: str) -> bool:
        return os.path.exists(os.path.join(self.cache_folder, *keys, filename))

    def read(self, keys: List[str], filename: str) -> TextIO:
        return open(os.path.join(self.cache_folder, *keys, filename), "r")

    def write(self, keys: List[str], filename: str) -> TextIO:
        folder = os.path.join(self.cache_folder, *keys)
        os.makedirs(folder, exist_ok=True)
        return open(os.path.join(folder, filename), "w")


@dataclass
class Result:
    test_name: str
    result: str

    def __hash__(self):
        return hash(self.test_name)

    def __lt__(self, other):
         return self.test_name < other.test_name

    def __str__(self):
        return f"{self.test_name},{self.result}"

    def __repr__(self):
        return str(self)


@dataclass
class Pipeline:
    raw_attrs: Dict[str, Any]

    def __getattr__(self, name):
        return self.raw_attrs.get(name)


# HACK: do not use pydantic as it crashes for unknown reasons
@dc
class Job:
    raw_attrs: Dict[str, Any]
    log: str = None
    log_format: str = None
    pre_merge: bool = False

    project: InitVar["Project"] = None
    is_new: InitVar[bool] = False

    def __post_init__(self, project, is_new):
        self.project = project
        self.is_new = is_new

        if isinstance(self.log, bytes):
            self.log = self.log.decode()

    def _to_datetime(self, value):
        if value is not None:
            return datetime.fromisoformat(value.rstrip("Z"))

    @property
    def created_at(self):
        return self._to_datetime(self.raw_attrs.get("created_at"))

    @property
    def finished_at(self):
        return self._to_datetime(self.raw_attrs.get("finished_at"))

    @property
    def queued_duration(self):
        return timedelta(seconds=self.raw_attrs.get("queued_duration"))

    @property
    def duration(self):
        return timedelta(seconds=self.raw_attrs.get("duration"))

    @property
    def runner_name(self):
        return self.runner.get("description")

    @cached_property
    def dut_name(self):
        results = re.findall(r"b2c.hostname=(\S+) ", self.log)
        if len(results) > 0 and len(results[-1]) > 6:
            return results[-1]
        else:
            # If we didn't find the hostname, it means the problem was the runner itself
            return self.runner_name

    @property
    def runner_tags(self):
        return self.tag_list

    @property
    def pipeline_id(self):
        return self.raw_attrs["pipeline"]["id"]

    def _parse_deqp_runner_logs(self):
        known_flakes_start_re = re.compile(r"^(Some known flakes|Some flaky tests) found:")
        undocumented_flakes_start_re = re.compile(r"^Some new flakes found:")
        unexpected_results_start_re = re.compile(r"^(Some failures found|Unexpected results):")

        unexpected_results = []
        known_flakes = []
        undocumented_flakes = []
        log_truncated = False
        cur_section = None
        for line in self.log.splitlines():
            if cur_section is not None:
                if line.startswith("  "):
                    fields = line.strip().split(",")
                    if cur_section in [known_flakes, undocumented_flakes] and len(fields) == 1:
                        cur_section.append(fields[0])
                    elif cur_section == unexpected_results and len(fields) == 2:
                        cur_section.append(Result(test_name=fields[0], result=fields[1]))
                    else:
                        cur_section = None
                else:
                    cur_section = None

            if known_flakes_start_re.match(line):
                cur_section = known_flakes
            elif undocumented_flakes_start_re.match(line):
                cur_section = undocumented_flakes
            elif unexpected_results_start_re.match(line):
                cur_section = unexpected_results
            elif line.startswith("Job execution will continue but no more output will be collected"):
                log_truncated = True

        if log_truncated:
            unexpected_results.append(Result(test_name="job-log", result="truncated"))

        # Reboots are unexpected, so let's mark them as failures
        if self.reboot_count > 0:
            unexpected_results.append(Result(test_name="reboot_count", result=f"{self.reboot_count}"))

        return known_flakes, undocumented_flakes, unexpected_results

    def _parse_vkd3d_logs(self):
        if result := re.search(r"\nhwci: mesa: (pass|fail)", self.log):
            if result.groups()[0] == 'fail':
                return ["vkd3d-changes"], [], []

        return [], [], []

    @cached_property
    def _parse_logs(self):
        if self.log_format == "deqp_runner":
            return self._parse_deqp_runner_logs()
        elif self.log_format == "vkd3d":
            return self._parse_vkd3d_logs()

        return [], [], []

    @property
    def known_flakes(self):
        return self._parse_logs[0]

    @property
    def undocumented_flakes(self):
        return self._parse_logs[1]

    @property
    def unexpected_results(self):
        return self._parse_logs[2]

    @property
    def failures(self):
        return self.unexpected_results

    @property
    def flakes(self):
        return self.known_flakes + self.undocumented_flakes

    @property
    def has_failures(self):
        return len(self.failures) > 0

    @cached_property
    def infra_issues(self):
        return TriageResult(self.log, types=TriageType.INFRA)

    @cached_property
    def job_issues(self):
        return TriageResult(self.log, types=TriageType.JOB)

    @property
    def reboot_count(self):
        return len(self.job_issues.matched_patterns.get('JOB_REBOOT', []))

    @property
    def needs_attention(self):
        if self.infra_issues.requires_attention or self.job_issues.requires_attention:
            return True

        # Check if we have an unexplained failure
        return self.status == "failure" and len(self.failures) == 0 and not self.infra_issues.has_known_issues and not self.job_issues.has_known_issues

    def __getattr__(self, name):
        # See https://docs.gitlab.com/ee/api/jobs.html for more information about the data available
        return self.raw_attrs.get(name)

    def __hash__(self):
        return hash((self.pipeline.get("project_id"), self.id))


class Project:
    def __init__(self, name: str, config: Config, cache: Cache, since: datetime, cache_only: bool = False):
        self.name = name
        self.config = config
        self.cache = cache
        self.since = since
        self.cache_only = cache_only

        self._merge_base_cache = dict()

    @property
    def project_config(self):
        return self.config.projects.get(self.name)

    @cached_property
    def gl_project(self):
        gl = gitlab.Gitlab(url=self.project_config.gitlab.instance_url,
                           private_token=self.project_config.gitlab.project_access_token,
                           retry_transient_errors=True)
        return gl.projects.get(self.project_config.gitlab.project_id)

    @cached_property
    def default_branch(self):
        return self.gl_project.default_branch

    def _merge_base(self, refs):
        tuple_refs = tuple(refs)
        if base := self._merge_base_cache.get(tuple_refs):
            return base
        else:
            base = self.gl_project.repository_merge_base(tuple_refs).get('id')
            self._merge_base_cache[tuple_refs] = base
            return base

    def is_ref_merged(self, ref):
        return self._merge_base([ref, self.default_branch]) == ref

    @cached_property
    def cached_jobs_of_interest(self):
        def is_job_of_interest(job):
            for cfg_job in self.project_config.find_compatible_jobs(job.pipeline.get('ref')):
                if cfg_job.is_job_selectable(job):
                    return True
            return False

        jobs = list()
        for pipeline_id in self.cache.list_keys([self.name]):
            for job_id in self.cache.list_keys([self.name, pipeline_id]):
                try:
                    with self.cache.read([self.name, pipeline_id, job_id], filename="finished_at") as f:
                        finished_at = datetime.fromisoformat(f.read())
                        if finished_at is None or finished_at < self.since:
                            continue

                    with self.cache.read([self.name, pipeline_id, job_id], filename="job.yml") as f:
                        job = Job(project=self, **yaml.safe_load(f))
                        job.is_new = False
                        if is_job_of_interest(job):
                            jobs.append(job)
                except Exception as e:
                    traceback.print_exc()
                    elog(f"ERROR: can't load the cached job id {job_id}: {e}")
        return jobs

    def _find_latest_pipeline_updates(self, since) -> list[gitlab.v4.objects.ProjectPipeline]:
        ilog(f"{self.name}: Fetching all the pipelines updated after {since}")

        # Process pipelines from oldest to newest
        pipelines = self.gl_project.pipelines.list(updated_after=rfc3339_format(since), all=True, sort='asc')
        return [p for p in pipelines if len(self.project_config.find_compatible_jobs(p.ref)) > 0]

    @cached_property
    def jobs_of_interest(self) -> list["Project.Job"]:
        # Load all the cached jobs that fit the description
        start = time.time()
        jobs = self.cached_jobs_of_interest
        finished_cached_jobs = {j.id:j for j in jobs if j.finished_at}

        # Figure out what is the last job we have in our cache
        newest_entry_in_cache = self.since
        oldest_entry_in_cache = self.since
        for job in jobs:
            newest_entry_in_cache = max(newest_entry_in_cache, job.finished_at)
            oldest_entry_in_cache = min(oldest_entry_in_cache, job.finished_at)

        ilog(f"{self.name}: Found {len(jobs)} cached jobs fitting the request, oldest={oldest_entry_in_cache} and newest={newest_entry_in_cache} (took {time.time() - start:.1f} seconds)")

        if self.cache_only:
            return jobs

        # Make sure that all the cached jobs have an accurate pre_merge status
        ilog(f"{self.name}: Ensuring all the cached jobs have up-to-date pre-merge statuses")
        for job in jobs:
            is_pre_merge = not self.is_ref_merged(job.pipeline['sha'])
            if job.pre_merge != is_pre_merge:
                ilog(f"{self.name}: Updating #{job.pipeline['id']}'s {job.name}: Pre_merge {job.pre_merge} -> {is_pre_merge} ({job.web_url})")
                job.pre_merge = is_pre_merge
                job.is_new = True

        # Check that we are not trying to add new pipelines
        fetch_from = newest_entry_in_cache if oldest_entry_in_cache >= self.since else self.since

        # For extra safety, let's rewind time an extra minute so that we can
        # be sure that no jobs get through the cracks
        fetch_from -= timedelta(minutes=1)

        # Get all the latest jobs
        # NOTE: I wish we could use project.jobs.list(order_by="finished_at", sort_by="desc"),
        # but this is unsupported, so we to go through pipelines first...
        for pipeline in self._find_latest_pipeline_updates(fetch_from):
            pre_merge_str = "post-merge" if self.is_ref_merged(pipeline.sha) else "pre-merge"
            ilog(f"{self.name}: Fetching the finished jobs of {pre_merge_str} pipeline #{pipeline.id} (created_at={pipeline.created_at}, updated at={pipeline.updated_at})")
            finished_jobs = []

            # A quirk of the gitlab binding: we can't provide a list of scopes...
            for scope in ["success", "failed"]:
                finished_jobs.extend(pipeline.jobs.list(scope=scope, include_retried=True, all=True))
            ilog(f"  --> Found {len(finished_jobs)} finished jobs - {pipeline.web_url}")

            pipeline_jobs = []
            for job in finished_jobs:
                # Ignore jobs created before the since date
                if job.finished_at is None or datetime.fromisoformat(job.finished_at.rstrip("Z")) < self.since:
                    continue

                for cfg_job in self.project_config.find_compatible_jobs(pipeline.ref):
                    if cfg_job.is_job_selectable(job):
                        if job_obj := finished_cached_jobs.get(job.id):
                            # Re-use the version from the cache, since it is in its final form
                            dlog(f"Re-used job id {job.id} from the cache")
                            pass
                        else:
                            gl_job = self.gl_project.jobs.get(id=job.id, lazy=True)
                            if gl_job is None:
                                elog(f"Failed to fetch the GitLab job id {job.id}")
                                continue
                            else:
                                job_obj = Job(project=self, is_new=True,
                                            log=gl_job.trace(),
                                            pre_merge=not self.is_ref_merged(pipeline.sha),
                                            raw_attrs=job._attrs,
                                            log_format=cfg_job.log_format)
                        pipeline_jobs.append(job_obj)

            if len(finished_jobs) == 0:
                continue

            matched_jobs = ", ".join([j.name for j in pipeline_jobs])
            ilog(f"  --> Matched {len(pipeline_jobs)} / {len(finished_jobs)} jobs:")
            for j in pipeline_jobs:
                try:
                    if len(j.failures) > 0:
                        job_desc = ",".join([r.test_name for r in j.failures])
                    else:
                        job_desc = "No failures found"
                except Exception:
                    traceback.print_exc()
                    job_desc = "<Parse error>"
                ilog(f"\t {j.name} ({j.reboot_count} reboots): {job_desc}")
            jobs.extend(pipeline_jobs)

        return jobs

    def cache_resources_of_interest(self):
        for job in self.jobs_of_interest:
            if job.is_new:
                key = [self.name, str(job.pipeline_id), str(job.id)]
                with self.cache.write(key, "finished_at") as f:
                    f.write(f"{job.finished_at.isoformat()}")

                with self.cache.write(key, "job.yml") as f:
                    yaml.dump(asdict(job), f)

    def push_results_to_influx(self) -> None:
        if self.config.influxdb is None:
            return

        client = InfluxDBClient(url=self.config.influxdb.url, token=self.config.influxdb.access_token)
        write_api = client.write_api(write_options=SYNCHRONOUS)

        sequence = []
        for job in self.jobs_of_interest:
            # Pre-check: make sure that all the fields exist
            if job.runner_name is None:
                continue

            try:
                username = job.user.get("username", "UNKNOWN")
                point = (Point("job")
                            .tag("project", self.name)
                            .tag("name", job.name)
                            .tag("runner", job.runner_name)
                            .tag("dut", job.dut_name)
                            .field("pipeline_id", job.pipeline_id)
                            .field("pre_merge", job.pre_merge)
                            .field("user", username)
                            .field("job_id", job.id)
                            .field("status", job.status)
                            .field("failures", len(job.failures))
                            .field("flakes", len(job.flakes))
                            .field("needs_attention", job.needs_attention)
                            .field("queue_duration", job.queued_duration.total_seconds())
                            .field("duration", job.duration.total_seconds())
                            .field("url", job.web_url)
                            .time(job.created_at, WritePrecision.NS))

                # Add all the possible infra issues we have
                for matcher, issues in job.infra_issues.results.items():
                    point.field(matcher.name, len(issues))

                # Add all the possible job issues we have
                for matcher, issues in job.job_issues.results.items():
                    point.field(matcher.name, len(issues))

                sequence.append(point)
            except Exception as e:
                elog(f"ERROR: failed to generate a data point for Job id {job.id}: {traceback.format_exc()}\n")

        ilog("Batch writing %d measurements to Influx..." % len(sequence))
        write_api.write(self.config.influxdb.bucket, self.config.influxdb.org, sequence)


class Report:
    def __init__(self, jobs=None):
        self.jobs = jobs if jobs else []

    @property
    def is_empty(self):
        return len(self.jobs) > 0

    @property
    def sorted_by_completion_date(self):
        return Report(sorted(self.jobs, key=lambda j: j.finished_at, reverse=True))

    @property
    def sorted_by_creation_date(self):
        return Report(sorted(self.jobs, key=lambda j: j.created_at, reverse=True))

    @cached_property
    def new(self):
        return Report([j for j in self.jobs if j.is_new])

    @cached_property
    def pre_merge(self):
        return Report([j for j in self.jobs if j.pre_merge])

    @cached_property
    def post_merge(self):
        return Report([j for j in self.jobs if not j.pre_merge])

    @cached_property
    def needs_attention(self):
        return Report([j for j in self.jobs if j.needs_attention])

    @cached_property
    def infra_issues(self):
        return Report([j for j in self.jobs if j.infra_issues.requires_attention])

    @cached_property
    def job_issues(self):
        return Report([j for j in self.jobs if j.job_issues.requires_attention])

    @cached_property
    def failures(self):
        return Report([j for j in self.jobs if j.has_failures])

    def __create_subresult(self, attr, key_as_tuple=True, sort_keys=False):
        results = defaultdict(list)
        for job in self.jobs:
            key = getattr(job, attr)
            if isinstance(key, list) or isinstance(key, set):
                if key_as_tuple:
                   key = tuple(key)
                   results[key].append(job)
                else:
                    for item in key:
                        results[item].append(job)
            else:
                results[key].append(job)

        if sort_keys:
            results = { k:results[k] for k in sorted(results.keys())}

        return defaultdict(Report, {k:Report(jobs) for k, jobs in results.items()})

    @cached_property
    def duts(self):
        return self.__create_subresult("dut_name")

    @cached_property
    def runners(self):
        return self.__create_subresult("runner_name")

    @cached_property
    def runner_tags(self):
        return self.__create_subresult("runner_tags")

    @cached_property
    def projects(self):
        return self.__create_subresult("project")

    @cached_property
    def log_formats(self):
        return self.__create_subresult("log_format")

    @cached_property
    def job_name(self):
        return self.__create_subresult("name")

    @cached_property
    def flaky_tests(self):
        return self.__create_subresult("flakes", key_as_tuple=False, sort_keys=True)

    @cached_property
    def undocumented_flaky_tests(self):
        return self.__create_subresult("undocumented_flakes", key_as_tuple=False, sort_keys=True)

    @cached_property
    def known_flaky_tests(self):
        return self.__create_subresult("known_flakes", key_as_tuple=False, sort_keys=True)

    @cached_property
    def failed_tests(self):
        return self.__create_subresult("unexpected_results", key_as_tuple=False, sort_keys=True)

    @cached_property
    def pipelines(self):
        pipelines = defaultdict(list)
        for j in self.jobs:
            pipelines[j.pipeline['id']].append(j)
        return { p_id:pipelines[p_id] for p_id in sorted(pipelines.keys(), reverse=True) }

class InfluxChecks:
    def __init__(self, influx_cfg):
        self.influx_cfg = influx_cfg

    @cached_property
    def all(self):
        if self.influx_cfg is not None:
            return self.influx_cfg.checks.to_influx_checks()
        else:
            return []

    @property
    def failed(self):
        return [c for c in self.all if not c.result]

    def run(self):
        if self.influx_cfg is not None:
            influx = InfluxDB(url=self.influx_cfg.url, token=self.influx_cfg.access_token, org=self.influx_cfg.org)

            result = True
            for check in self.all:
                result &= check.run(influx)
            return result


def render_template(template, **kwargs):
    environment = jinja2.Environment(loader=jinja2.FileSystemLoader("./templates/"), trim_blocks=True)
    template = environment.get_template(template)
    return template.render(**kwargs)


def cmd_triage(args: argparse.Namespace):
    since = datetime.now() - timedelta_from_args_delta(args.since)

    config = Config.from_file(args.config_file)
    cache = Cache(args.pipeline_cache_dir)

    # Get the list of jobs of interest by going through the different projects
    jobs_of_interest = list()
    for project_name, project_config in config.projects.items():
        if args.projects and len(args.projects) > 0 and project_name not in args.projects:
            ilog(f"Ignoring project {project_name}")
            continue

        project = Project(name=project_name, config=config, cache=cache, since=since, cache_only=args.cache_only)
        project.cache_resources_of_interest()

        if not args.local_only:
            project.push_results_to_influx()

        jobs_of_interest.extend(project.jobs_of_interest)

    report = Report(jobs_of_interest)

    # Influx checks
    if not args.local_only:
        ilog("Run the Influxdb checks")
        influx_checks = InfluxChecks(config.influxdb)
        influx_checks.run()
    else:
        influx_checks = None

    # Templates
    envvars = {
        "JOB_URL": os.environ.get("CI_JOB_URL"),
    }

    ilog(render_template("stdout.txt.j2", report=report, env=envvars, influx_checks=None,
                         title=f"Execution report - All jobs since {since}"))
    ilog(render_template("stdout.txt.j2", report=report.new, env=envvars, influx_checks=influx_checks,
                         title=f"Execution report - Jobs since {since}"))

    # Notify people on IRC
    if not args.local_only and (len(report.new.infra_issues.jobs) > 0 or len(report.new.post_merge.failed_tests) > 0 or len(report.new.post_merge.undocumented_flaky_tests) > 0 or len(influx_checks.failed) > 0):
        for config_irc in config.notifications.irc.values():
            client = IRCClient(host=config_irc.hostname, port=config_irc.port,
                               channel=config_irc.channel, nick=config_irc.nickname)
            client.send_msg(render_template("irc.txt.j2", report=report, config_irc=config_irc,
                                            env=envvars, influx_checks=influx_checks))
            client.quit()

# TODO:
# - Add support for the fossils jobs
# - Add information about reproduction rate for failures

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--config-file", required=True, type=str)
    parser.add_argument("--log-file", default=None, type=str)
    parser.add_argument("--log-level", default="DEBUG", type=str)
    subparsers = parser.add_subparsers()

    triage_parser = subparsers.add_parser(
        "triage",
        help="Fetch all pipelines in a given period, and generate a triaging report. Optionally notify administrators when job failures are encountered.",
    )
    triage_parser.add_argument(
        "--pipeline-cache-dir",
        type=PathType(exists=True, type="dir"),
        required=True,
        help="Local directory into which to cache the pipeline's job information from the remote Gitlab server",
    )
    triage_parser.add_argument(
        "--since",
        default="1 days",
        type=str,
        help="Only fetch information for pipelines since the given time. Format is 'N {minutes,hours,days,weeks}'.",
    )
    triage_parser.add_argument("--cache-only", action='store_true', help="Do not download any new pipelines/jobs")
    triage_parser.add_argument("--project", dest="projects", nargs='+', help="Limit triage to the following project(s)")
    triage_parser.add_argument("--local-only", action="store_true", help="Do not upload any result anywhere. Useful for development.")
    triage_parser.set_defaults(func=cmd_triage)

    args = parser.parse_args()

    lvl = args.log_level.upper()
    if lvl not in LogLevel._member_names_:
        raise argparse.ArgumentTypeError("--log-level must be one of %s" % " or ".join(LogLevel._member_names_))
    global LOG_LEVEL
    LOG_LEVEL = getattr(LogLevel, lvl)

    if hasattr(args, "func"):
        args.func(args)
    else:
        parser.print_help()


if __name__ == "__main__":
    main()
