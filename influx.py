from dataclasses import dataclass
from functools import cached_property
from datetime import datetime, timedelta

from influxdb_client import InfluxDBClient, ChecksService


@dataclass
class InfluxCheck:
    name: str
    query: str
    result: bool = None

    def run(self, client):
        raise NotImplementedError()

    @property
    def human_result(self):
        return "OK" if self.result else "FAIL"

    def __str__(self):
        return f'<{self.name}: {self.human_result}>'


class InfluxDeadmanCheck(InfluxCheck):
    def __init__(self, hostname, bucket, last_seen_seconds="90s"):
        query = f"""from(bucket: "{bucket}")
  |> range(start: -{last_seen_seconds}s)
  |> filter(fn: (r) => r["_measurement"] == "system")
  |> filter(fn: (r) => r["_field"] == "uptime")
  |> filter(fn: (r) => r["host"] == "{hostname}")
  |> top(n: 1)
"""
        super().__init__(name=f"Deadman switch for hostname {hostname}", query=query)

    def run(self, client):
        if len(client.query(self.query)) > 0:
            self.result = True
        else:
            self.result = False

        return self.result

    @property
    def human_result(self):
        return "ONLINE" if self.result else "DOWN"


class InfluxDB:
    def __init__(self, url, token, org, refresh_rate=timedelta(seconds=60)):
        self.url = url
        self.token = token
        self.org = org

    @cached_property
    def client(self):
        return InfluxDBClient(url=self.url, token=self.token, org=self.org)

    def query(self, text):
        return self.client.query_api().query(text)
