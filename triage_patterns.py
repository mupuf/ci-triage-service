from dataclasses import dataclass
from functools import cached_property
from enum import Flag, auto
import re
from typing import Optional


class TriageType(Flag):
    INFRA = auto()
    JOB = auto()


@dataclass
class MatcherAction:
    name: str
    description: str
    type: TriageType
    text: Optional[str] = None
    regex: Optional[re.Pattern] = None
    requires_attention: bool = True

    def __post_init__(self):
        # Exactly one of text or regex must be provided
        assert (self.text is None) != (self.regex is None)

        if self.text is not None:
            self.regex = re.compile(re.escape(self.text))

    def __hash__(self):
        return hash(self.name)

    def match_line(self, line):
        if m := self.regex.search(line):
            return Match(self, m)


@dataclass
class Match:
    matcher: MatcherAction
    match: re.Match


MATCHER_TABLE = [
    MatcherAction(
        name="AMDGPU_ENOMEM",
        description="Amdgpu ran out of space for command submission",
        text="*ERROR* Not enough memory for command submission!",
        type=TriageType.JOB,
        requires_attention=False,
    ),
    MatcherAction(
        name="BOOT_FAILURE",
        description="First console activity timeout",
        regex=re.compile(r"Hit the timeout <Timeout first_console_activity.*--> Abort!"),
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="CONTAINER_ERROR",
        description="Container build / runtime issue",
        regex=re.compile(
            r"error committing container|error pushing image|Error processing tar file|http: server gave HTTP response to HTTPS client|writing blob: adding layer with blob .*: layer not known|Cannot connect to the Docker daemon at unix:///var/run/docker\.sock\. Is the docker daemon running\?"
        ),
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="DEQP_PARSE_ERROR",
        description="dEQP caselists parse error",
        text="Error: Failed to parse dEQP",
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="AMDGPU_HUNG",
        description="AMDGPU graphics context hung",
        text="[drm:amdgpu_job_timedout]",
        type=TriageType.JOB,
    ),
    MatcherAction(
        name="EXECUTOR_ISSUE",
        description="Executor bug / issue",
        regex=re.compile(r"ValueError: The server failed to initiate a connection|Internal Server Error"),
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="EXECUTOR_TIMEOUT",
        description="One of the timeouts got hit",
        text="Hit the timeout <Timeout",
        type=TriageType.JOB,
    ),
    MatcherAction(
        name="GITLAB_GENERIC_JOB_FAILURE",
        description="Gitlab related infra failure",
        regex=re.compile(
            r'requests.exceptions.HTTPError: 404 Client Error: Not Found for url: https://gitlab.freedesktop.org|Job failed \(system failure\)|Uploading artifacts.*Bad Gateway|ERROR: Uploading artifacts as "archive" to coordinator.*413 Request Entity Too Large|ERROR: Uploading artifacts as "archive" to coordinator... error.*couldn\'t execute POST against|HTTP request sent, awaiting response... 500 Internal Server Error|error: RPC failed;|section_end:1631042648:upload_artifacts_on_failure'
        ),
        type=TriageType.INFRA,
        requires_attention=False,
    ),
    MatcherAction(
        name="JOB_TIMEOUT",
        description="Test timed out",
        regex=re.compile(r"ERROR: Job failed: execution took longer than \\d+h\\d+m\\d+s seconds|ERROR: Job failed: execution took longer than"),
        type=TriageType.JOB,
    ),
    MatcherAction(
        name="JOB_REBOOT",
        description="Job had to be restarted",
        regex=re.compile(r"Matched the following patterns:.*session_reboot"),
        type=TriageType.JOB,
    ),
    MatcherAction(
        name="MINIO_ERROR",
        description="minio generic error",
        text="mcli: <ERROR>",
        type=TriageType.INFRA,
        requires_attention=False,
    ),
    MatcherAction(
        name="NO_SPACE_LEFT",
        description="No space left",
        text="No space left on device",
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="PDU_BUG",
        description="Failure interacting with the PDU",
        regex=re.compile(r"AttributeError: 'NoneType' object has no attribute 'set'|The function <function SnmpPDU.get_port_state .*> failed 3 times in a row"),
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="MISMATCHING_MARS_TAGS",
        description="Machine tags mismatch",
        text="Mismatch for 'tags'",
        type=TriageType.INFRA,
    ),
     MatcherAction(
        name="STALE_CLIENT_BUG",
        description="A client is already connected, re-try later!",
        text="A client is already connected, re-try later!",
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="TARFILE_BUG_FAILURE",
        description="Job folder sync bug detected",
        text="tarfile.ReadError: file could not be opened successfully",
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="TIMEOUT_NONE_BUG",
        description="Timeout none bug detected",
        text="AttributeError: 'NoneType' object has no attribute 'timeouts'",
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="UNAUTHORIZED_USE_ERROR",
        description="Unauthorized user error",
        text="The machines exposed by this runner are only allowed to be used by Valve-authorized projects",
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="WGET_404_ERROR",
        description="wget fetch failure",
        text="ERROR 404: Not Found",
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="JOB_PROCESS_FAILED_TO_START",
        description="The Job Process failed to start",
        text='ERROR: Could not queue the job: "The job process failed to start"',
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="EXECUTOR_SETUP_FAIL",
        description="The job status was SETUP_FAIL",
        text='run_job: status: JobStatus.SETUP_FAIL',
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="EXECUTOR_DOWN",
        description="Executorctl couldn't connect to the executor",
        text='Failed to connect to the executor, is it running?',
        type=TriageType.INFRA,
    ),
    MatcherAction(
        name="KERNEL_NULL_PTR_DEREF",
        description="The kernel hit a NULL pointer dereference",
        regex=re.compile(r'\[[ \d\.]+\] BUG: kernel NULL pointer dereference'),
        type=TriageType.JOB,
    ),
    MatcherAction(
        name="KERNEL_GENERAL_PROTECTION_FAULT",
        description="The kernel hit a general protection fault",
        regex=re.compile(r'\[[ \d\.]+\] general protection fault'),
        type=TriageType.JOB,
    ),

    # TODO: Detect the dxvk-ci issue where VRAM is too small
]

class TriageResult:
    def __init__(self, job_log: str, types=TriageType.INFRA|TriageType.JOB):
        self.matches = []

        for line in job_log.splitlines():
            for ma in [ma for ma in MATCHER_TABLE if types in ma.type]:
                if match := ma.match_line(line):
                    self.matches.append(match)

    @cached_property
    def results(self):
        mas = {ma: [] for ma in MATCHER_TABLE}
        for match in self.matches:
            mas[match.matcher].append(match)
        return mas

    @cached_property
    def matched_patterns(self):
        return {matcher.name: matches for matcher, matches in self.results.items() if len(matches) > 0}

    @property
    def has_known_issues(self):
        return len(self.matches) > 0

    @property
    def requires_attention(self):
        for match in self.matches:
            if match.matcher.requires_attention:
                return True
        return False
